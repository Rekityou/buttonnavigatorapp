package com.tirado.buttonnavigator.Database

import android.provider.BaseColumns

class DefinirTabla {
    //clase estatica, no genera objetos, hace refencia con el nombre de la clase y atributos

    object Alumnos : BaseColumns{


        const val TABLA = "alumnos"
        const val ID = "id"
        const val MATRICULA = "matricula"
        const val NOMBRE = "nombre"
        const val DOMICILIO = "domicilio"
        const val ESPECIALIDAD = "especialidad"
        const val FOTO = "foto"
    }
}
