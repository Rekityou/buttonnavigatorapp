package com.tirado.buttonnavigator.Database

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Alumno(
    var id: Int = 0,
    var matricula: String = "",
    var nombre: String = "",
    var domicilio: String = "",
    var especialidad: String = "",
    var foto: String = ""
) : Parcelable
